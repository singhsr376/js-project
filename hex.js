const hex = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, "A", "B", "C", "D", "E", "F"];
//#f15025
const btn = document.getElementById("btn");
const btn2 = document.getElementById("btn2");
const btn3 = document.getElementById("btn3");
const color = document.querySelector(".color");

btn.addEventListener('click', function(){
    let hexcolor = "#";
    for(let i = 0; i<6; i++){
        hexcolor += hex[getRandomNumber()];
    }    
    color.textContent = hexcolor;
    document.body.style.background = hexcolor;
});

/*button2*/
btn2.addEventListener('click', function(){
    let hexcolor = "#";
    for(let j = 0; j<6; j++){
        hexcolor += hex[getRandomNumber()];
    }    
    color.textContent = hexcolor;
    document.getElementById("btn").style.background = hexcolor;
});

/*button3*/
btn3.addEventListener('click', function(){
    let hexcolor = "#";
    for(let j = 0; j<6; j++){
        hexcolor += hex[getRandomNumber()];
    }    
    color.textContent = hexcolor;
    document.getElementById("btn3").style.background = hexcolor;
});


function getRandomNumber() {
    return Math.floor(Math.random() * hex.length);
}
